<?php
/**
 * Plugin Name: Attachment Pages Redirect
 * Description: Makes attachment pages redirects 301 to its media URL. That helps with SEO thin content problem.
 * Version: 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'ATTACHMENT_REDIRECT_CODE' ) ) {
	define( 'ATTACHMENT_REDIRECT_CODE', '301' );
}


function dom_attachment_redirect() {
	global $post;
	if ( is_attachment() ) {
		wp_redirect(wp_get_attachment_url($post->ID), ATTACHMENT_REDIRECT_CODE);
		exit;
	}
}
add_action( 'template_redirect', 'dom_attachment_redirect', 1 );